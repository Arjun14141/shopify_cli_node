import { list } from "./controller/customer.controller";
// import { verifyRequest } from "@shopify/koa-shopify-auth";

export function defaultRoutes(router) {
  //customer
  router.get(
    "/customer/list",
    // verifyRequest({ returnHeader: true }),
    async (ctx, next) => list(ctx, next)
  );
}
