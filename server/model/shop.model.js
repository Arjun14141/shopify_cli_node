import { Schema, model } from "mongoose";

const shopSchema = Schema({
  shop: String,
  accessToken: String,
  expires: Date,
  scope: String,
});

const Shop = model("Shop", shopSchema);
export default Shop;
