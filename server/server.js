import "@babel/polyfill";
import createShopifyAuth, { verifyRequest } from "@shopify/koa-shopify-auth";
import Shopify from "@shopify/shopify-api";
import dotenv from "dotenv";
import "isomorphic-fetch";
import Koa from "koa";
import Router from "koa-router";
import koaBody from "koa-body";
import mongoose from "mongoose";
import next from "next";
import { defaultRoutes } from "./route";
const Shop = require("./model/shop.model").default;
dotenv.config();
const port = parseInt(process.env.PORT, 10) || 7778;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

Shopify.Context.initialize({
  API_KEY: process.env.SHOPIFY_API_KEY,
  API_SECRET_KEY: process.env.SHOPIFY_API_SECRET,
  SCOPES: process.env.SCOPES.split(","),
  HOST_NAME: process.env.HOST.replace(/https:\/\/|\/$/g, ""),
  API_VERSION: "2022-01",
  IS_EMBEDDED_APP: true,
  // This should be replaced with your preferred storage strategy
  SESSION_STORAGE: new Shopify.Session.MemorySessionStorage(),
});

// persist this object in your app.

Shopify.Webhooks.Registry.addHandler("APP_UNINSTALLED", {
  path: "/webhooks",
  webhookHandler: async (_topic, shop, _body) => {
    console.log("APP_UNINSTALLED :", shop);
    if (shop)
      await Shop.deleteOne({ shop })
        .then(() => console.log(`${shop} remove from db`))
        .catch((err) => console.log(`error while remove ${shop}: ${err}`));
  },
});

app.prepare().then(async () => {
  const server = new Koa();
  // koa-body act as bodyparser
  server.use(koaBody());
  const router = new Router();
  server.keys = [Shopify.Context.API_SECRET_KEY];
  server.use(
    createShopifyAuth({
      async afterAuth(ctx) {
        // Access token and shop available in ctx.state.shopify
        const { shop, accessToken, scope, expires } = ctx.state.shopify;
        const host = ctx.query.host;
        // console.log("start", ctx.state.shopify);
        // add shop to db if not available
        if (shop) {
          const shopOne = await Shop.findOne({ shop });
          if (!shopOne)
            await Shop.create({ shop, accessToken, scope, expires })
              .then(() => console.log(`${shop} added to database`))
              .catch((err) =>
                console.log(`error while adding ${shop}: ${err}`)
              );
          else {
            if (shopOne.accessToken !== accessToken) {
              Object.assign(shopOne, { accessToken, expires });
              await shopOne.save();
            }
          }
        }

        const responses = await Shopify.Webhooks.Registry.register({
          shop,
          accessToken,
          path: "/webhooks",
          topic: "APP_UNINSTALLED",
        });

        if (!responses["APP_UNINSTALLED"].success)
          console.log(
            `Failed to register APP_UNINSTALLED webhook: ${responses.result}`
          );
        // Redirect to app with shop parameter upon auth
        ctx.redirect(`/?shop=${shop}&host=${host}`);
      },
    })
  );

  const handleRequest = async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  };

  router.post("/webhooks", async (ctx) => {
    try {
      await Shopify.Webhooks.Registry.process(ctx.req, ctx.res);
      console.log(`Webhook processed, returned status code 200`);
    } catch (error) {
      console.log(`Failed to process webhook: ${error}`);
    }
  });

  router.post(
    "/graphql",
    verifyRequest({ returnHeader: true }),
    async (ctx) => await Shopify.Utils.graphqlProxy(ctx.req, ctx.res)
  );

  router.get("(/_next/static/.*)", handleRequest); // Static content is clear
  router.get("/_next/webpack-hmr", handleRequest); // Webpack content is clear
  router.get("/", async (ctx) => {
    const shop = ctx.query.shop;
    // This shop hasn't been seen yet, go through OAuth to create a session
    const shopOne = await Shop.findOne({ shop });
    if (!shopOne) ctx.redirect(`/auth?shop=${shop}`);
    else await handleRequest(ctx);
  });
  // api route
  defaultRoutes(router);
  server.use(router.allowedMethods());
  server.use(router.routes());
  mongoose.connect(process.env.MONGO_URL).then(() => {
    console.log("Connected to MongoDB");
    server.listen(port, () => {
      console.log(`> Ready on http://localhost:${port}`);
    });
  });
  mongoose.Promise = require("bluebird");
  mongoose.connection.on("error", (err) => {
    console.error(`🚫 Database Error 🚫  → ${err}`);
  });
});
