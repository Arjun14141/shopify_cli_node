import Shopify from "@shopify/shopify-api";
const Shop = require("../model/shop.model").default;

module.exports = {
  async list(ctx, _next) {
    console.log("customer req", ctx.query, ctx.request.body);
    const shopOne = await Shop.findOne({ shop: ctx.query.shop });
    // const session = await Shopify.Utils.decodeSessionToken(
    //   ctx.headers.authorization.replace("Bearer ", "")
    // );
    // console.log("client:", session);
    if (shopOne) {
      // if (session) {
      const client = new Shopify.Clients.Rest(
        shopOne.shop,
        shopOne.accessToken
      );
      await client
        .get({ path: "customers" })
        .then((data) => {
          ctx.body = data;
          ctx.status = data.status;
        })
        .catch((err) => {
          ctx.body = err;
          ctx.status = err.code;
        });
    }
    // }
  },
};
