import { Provider, useAppBridge } from "@shopify/app-bridge-react";
import { authenticatedFetch, getSessionToken } from "@shopify/app-bridge-utils";
import { Redirect } from "@shopify/app-bridge/actions";
import { AppProvider } from "@shopify/polaris";
import "@shopify/polaris/dist/styles.css";
import translations from "@shopify/polaris/locales/en.json";
import ApolloClient from "apollo-boost";
import App from "next/app";
import React from "react";
import { ApolloProvider } from "react-apollo";

function userLoggedInFetch(app) {
  const fetchFunction = authenticatedFetch(app);
  return async (uri, options) => {
    const response = await fetchFunction(uri, options);
    if (
      response.headers.get("X-Shopify-API-Request-Failure-Reauthorize") === "1"
    ) {
      const authUrlHeader = response.headers.get(
        "X-Shopify-API-Request-Failure-Reauthorize-Url"
      );
      const redirect = Redirect.create(app);
      redirect.dispatch(Redirect.Action.APP, authUrlHeader || `/auth`);
      return null;
    }
    return response;
  };
}

function MyProvider(props) {
  const app = useAppBridge();
  const getToken = async () =>
    await getSessionToken(app)
      .then((token) => {
        if (token) window.session_token = token;
      })
      .catch((err) => console.log("session token:", err));

  const client = new ApolloClient({
    fetch: userLoggedInFetch(app),
    fetchOptions: { credentials: "include" },
  });
  const Component = props.Component;
  return (
    <ApolloProvider client={client}>
      <Component {...props} shop={props.shop} getToken={getToken} />
    </ApolloProvider>
  );
}

class MyApp extends App {
  render() {
    const { Component, pageProps, host, shop } = this.props;
    return (
      <AppProvider i18n={translations}>
        <Provider config={{ apiKey: API_KEY, host: host, forceRedirect: true }}>
          <MyProvider Component={Component} {...pageProps} shop={shop} />
        </Provider>
      </AppProvider>
    );
  }
}

MyApp.getInitialProps = async ({ ctx }) => {
  return { host: ctx.query.host, shop: ctx.query.shop };
};

export default MyApp;
