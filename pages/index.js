import { ResourcePicker, TitleBar } from "@shopify/app-bridge-react";
import { Button, Heading, Page } from "@shopify/polaris";
import { useState } from "react";
import { getCustomerList } from "./services/customer.service";

export default function Index(props) {
  const [open, setOpen] = useState(false);

  const primaryAction = { content: "Fooss", url: "/" };
  const secondaryActions = [{ content: "Bar", url: "/", loading: true }];
  const actionGroups = [
    { title: "Baz", actions: [{ content: "Baz", url: "/" }] },
  ];

  const cutomerHandler = async () => {
    props.getToken().then(() => {
      getCustomerList(props.shop)
        .then((data) => console.log(data, "qwertyuiop"))
        .catch((err) => console.log(err));
    });
  };

  return (
    <Page>
      <Heading>
        Shopify app with Node and React{" "}
        <span role="img" aria-label="tada emoji">
          🎉
        </span>
        <Button onClick={() => setOpen(!open)}>EXAMPLE</Button>
        <Button onClick={cutomerHandler}>EXAMPLE</Button>
        <ResourcePicker
          resourceType="Product"
          open={open}
          onClose={() => setOpen(false)}
        />
        <TitleBar
          title={props.shop}
          primaryAction={primaryAction}
          secondaryActions={secondaryActions}
          actionGroups={actionGroups}
        />
      </Heading>
    </Page>
  );
}
