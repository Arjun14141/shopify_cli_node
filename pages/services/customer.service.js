import instance from "./axios.interceptor";

module.exports = {
  async getCustomerList(shop) {
    if (!shop) return;
    const url = `customer/list?shop=${shop}`;
    return await instance.get(url);
  },

  async createCustomer(shop, customerData) {
    if (!shop || !customerData) return;
    const url = `customer?shop=${shop}`;
    return await instance.post(url, { body: JSON.stringify(customerData) });
  },
};
