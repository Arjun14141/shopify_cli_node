import axios from "axios";

const instance = axios.create();
// Intercept all requests on this Axios instance
instance.interceptors.request.use((req) => {
  console.log("token", window.session_token);
  if (window.session_token)
    req.headers["Authorization"] = `Bearer ${window.session_token}`;
  req.headers["Content-Type"] = "application/json";
  return req;
});
// Export your Axios instance to use within your app
export default instance;
